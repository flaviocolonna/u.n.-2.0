<?php
session_start();
require_once("database.php");
$email = $_POST['email'];
$password = $_POST['password'];
try{
$pdo = $databaseConnection->getPdo();
$sql = "SELECT nome,cognome,foto FROM utente WHERE email=:email AND password=:password";
$stmt = $pdo -> prepare($sql);
$stmt -> bindValue(':email',$email);
$stmt -> bindValue(':password',md5($password));
$stmt -> execute();
$result = $stmt->fetchALL();
if(count($result)>0){
	$_SESSION['nome']=$result[0][0];
	$_SESSION['cognome']=$result[0][1];
	$_SESSION['email']=$email;
	$_SESSION['image']=$result[0][2];
	$_SESSION['welcome']=false;
	header("Location: ../homepage.php");
}else{
	$_SESSION['error']=true;
	header("Location: ../index.php");
}
exit();
}catch(PDOException $e){
	echo "Errore database: ".$e->getMessage();
	exit();
}
?>