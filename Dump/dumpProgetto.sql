-- MySQL dump 10.13  Distrib 5.6.19, for linux-glibc2.5 (x86_64)
--
-- Host: 127.0.0.1    Database: unibo_project
-- ------------------------------------------------------
-- Server version	5.6.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `allegato`
--

DROP TABLE IF EXISTS `allegato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `allegato` (
  `idallegato` int(11) NOT NULL,
  `id_prog_allegato` int(11) NOT NULL,
  `file` blob NOT NULL,
  `screenshot` tinyint(1) NOT NULL,
  `documentazione` tinyint(1) NOT NULL,
  `sorgente` tinyint(1) NOT NULL,
  PRIMARY KEY (`idallegato`),
  KEY `fk_allegato_1_idx` (`id_prog_allegato`),
  CONSTRAINT `fk_allegato_1` FOREIGN KEY (`id_prog_allegato`) REFERENCES `progetto` (`idprogetto`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `appunti`
--

DROP TABLE IF EXISTS `appunti`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `appunti` (
  `idappunti` int(11) NOT NULL,
  `email_studente` varchar(45) NOT NULL,
  `descrizione` varchar(45) NOT NULL,
  `data_upload` datetime NOT NULL,
  `allegato` blob NOT NULL,
  `foto` tinyint(1) NOT NULL,
  `pdf` tinyint(1) NOT NULL,
  `mappa_mentale` tinyint(1) NOT NULL,
  PRIMARY KEY (`idappunti`),
  KEY `fk_appunti_1_idx` (`email_studente`),
  CONSTRAINT `fk_appunti_1` FOREIGN KEY (`email_studente`) REFERENCES `studente` (`email_studente`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `commenti`
--

DROP TABLE IF EXISTS `commenti`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `commenti` (
  `idcommento` int(11) NOT NULL,
  `idappunto` int(11) NOT NULL,
  `email_creatore` varchar(45) NOT NULL,
  `testo` varchar(45) NOT NULL,
  `data` datetime NOT NULL,
  `valutazione` int(4) NOT NULL,
  PRIMARY KEY (`idcommento`),
  KEY `fk_commenti_1_idx` (`email_creatore`),
  KEY `fk_commenti_2_idx` (`idappunto`),
  CONSTRAINT `fk_commenti_1` FOREIGN KEY (`email_creatore`) REFERENCES `studente` (`email_studente`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_commenti_2` FOREIGN KEY (`idappunto`) REFERENCES `appunti` (`idappunti`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `commissionato`
--

DROP TABLE IF EXISTS `commissionato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `commissionato` (
  `id_prog_commissionato` int(10) NOT NULL,
  `id_azienda` int(10) NOT NULL,
  PRIMARY KEY (`id_prog_commissionato`,`id_azienda`),
  KEY `fk_commissionato_2_idx` (`id_azienda`),
  CONSTRAINT `fk_commissionato_1` FOREIGN KEY (`id_prog_commissionato`) REFERENCES `progetto` (`idprogetto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_commissionato_2` FOREIGN KEY (`id_azienda`) REFERENCES `lista_aziende` (`idazienda`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `corso`
--

DROP TABLE IF EXISTS `corso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `corso` (
  `idcorso` int(11) NOT NULL,
  `email_doc` varchar(45) NOT NULL,
  `codice` int(10) NOT NULL,
  `anno` int(10) NOT NULL,
  `nome` varchar(45) NOT NULL,
  `numero_crediti` int(10) NOT NULL,
  `corso_laurea` set('informatica','informatica_management') NOT NULL,
  PRIMARY KEY (`idcorso`,`anno`,`email_doc`),
  KEY `fk_corso_1_idx` (`email_doc`),
  CONSTRAINT `fk_corso_1` FOREIGN KEY (`email_doc`) REFERENCES `professore` (`email_professore`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `elaborato`
--

DROP TABLE IF EXISTS `elaborato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elaborato` (
  `idelaborato` int(11) NOT NULL,
  `id_esercitazione` int(11) NOT NULL,
  `email_studente_elaborato` varchar(45) NOT NULL,
  `commento` varchar(45) NOT NULL,
  `allegato` blob NOT NULL,
  `data_upload` datetime NOT NULL,
  PRIMARY KEY (`idelaborato`),
  KEY `fk_elaborato_1_idx` (`id_esercitazione`),
  KEY `fk_elaborato_2_idx` (`email_studente_elaborato`),
  CONSTRAINT `fk_elaborato_1` FOREIGN KEY (`id_esercitazione`) REFERENCES `esercitazione` (`idesercitazione`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_elaborato_2` FOREIGN KEY (`email_studente_elaborato`) REFERENCES `studente` (`email_studente`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `esercitazione`
--

DROP TABLE IF EXISTS `esercitazione`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `esercitazione` (
  `idesercitazione` int(11) NOT NULL,
  `email_prof_eserc` varchar(45) NOT NULL,
  `id_corso` int(11) NOT NULL,
  `descrizione` varchar(45) NOT NULL,
  `data_inizio` date NOT NULL,
  `data_fine` date NOT NULL,
  `voto` varchar(45) NOT NULL,
  PRIMARY KEY (`idesercitazione`),
  KEY `fk_esercitazioni_1_idx` (`email_prof_eserc`),
  KEY `fk_esercitazione_1_idx` (`id_corso`),
  CONSTRAINT `fk_esercitazione_1` FOREIGN KEY (`id_corso`) REFERENCES `corso` (`idcorso`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_esercitazione_2` FOREIGN KEY (`email_prof_eserc`) REFERENCES `professore` (`email_professore`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `follower`
--

DROP TABLE IF EXISTS `follower`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `follower` (
  `email_follower` varchar(45) NOT NULL,
  `email_followed` varchar(45) NOT NULL,
  PRIMARY KEY (`email_follower`,`email_followed`),
  KEY `fk_follower_2_idx` (`email_followed`),
  CONSTRAINT `fk_follower_1` FOREIGN KEY (`email_follower`) REFERENCES `utente` (`email`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_follower_2` FOREIGN KEY (`email_followed`) REFERENCES `utente` (`email`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `inbox`
--

DROP TABLE IF EXISTS `inbox`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inbox` (
  `idmessaggio` int(11) NOT NULL,
  `email_mittente` varchar(45) NOT NULL,
  `subject` varchar(45) NOT NULL,
  `contenuto` varchar(45) NOT NULL,
  `data_invio` datetime NOT NULL,
  PRIMARY KEY (`idmessaggio`),
  KEY `fk_inbox_1_idx` (`email_mittente`),
  CONSTRAINT `fk_inbox_1` FOREIGN KEY (`email_mittente`) REFERENCES `utente` (`email`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lista_aziende`
--

DROP TABLE IF EXISTS `lista_aziende`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lista_aziende` (
  `idazienda` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) NOT NULL,
  `sito` varchar(45) NOT NULL,
  `indirizzo` varchar(45) NOT NULL,
  PRIMARY KEY (`idazienda`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news` (
  `idnews` int(11) NOT NULL,
  `email_prof` varchar(45) NOT NULL,
  `subject` varchar(45) NOT NULL,
  `stringa_testo` varchar(45) NOT NULL,
  `data_inserimento` datetime NOT NULL,
  PRIMARY KEY (`idnews`),
  KEY `fk_news_1_idx` (`email_prof`),
  CONSTRAINT `fk_news_1` FOREIGN KEY (`email_prof`) REFERENCES `professore` (`email_professore`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `partecipa`
--

DROP TABLE IF EXISTS `partecipa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partecipa` (
  `email_stud_partecipante` varchar(45) NOT NULL,
  `id_progetto` int(11) NOT NULL,
  PRIMARY KEY (`email_stud_partecipante`,`id_progetto`),
  KEY `fk_corr_progetto_1_idx` (`id_progetto`),
  KEY `fk_partecipa_1_idx` (`email_stud_partecipante`),
  CONSTRAINT `fk_partecipa_1` FOREIGN KEY (`id_progetto`) REFERENCES `progetto` (`idprogetto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_partecipa_2` FOREIGN KEY (`email_stud_partecipante`) REFERENCES `studente` (`email_studente`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `professore`
--

DROP TABLE IF EXISTS `professore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `professore` (
  `email_professore` varchar(45) NOT NULL,
  `ruolo` set('ricercatore','associato','ordinario') NOT NULL,
  PRIMARY KEY (`email_professore`),
  CONSTRAINT `fk_professore_1` FOREIGN KEY (`email_professore`) REFERENCES `utente` (`email`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `progetto`
--

DROP TABLE IF EXISTS `progetto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `progetto` (
  `idprogetto` int(11) NOT NULL,
  `nome` varchar(45) NOT NULL,
  `descrizione` varchar(45) NOT NULL,
  `data_inizio` date NOT NULL,
  `num_partecipanti` int(10) NOT NULL,
  `tipo` set('extra','universitari') NOT NULL,
  `voto` int(10) NOT NULL COMMENT 'Questa tabella è inerente ai progetti. Ogni progetto ha dei dettagli comuni ma poi possono esserci due tipi di progetti: universitari ed extra.',
  `categoria` varchar(45) NOT NULL,
  `codice_corso` int(11) NOT NULL,
  `anno_accademico` int(10) NOT NULL,
  `email_docente` varchar(45) NOT NULL,
  PRIMARY KEY (`idprogetto`,`codice_corso`,`anno_accademico`,`email_docente`),
  KEY `fk_progetto_1_idx` (`codice_corso`),
  KEY `fk_progetto_2_idx` (`email_docente`),
  KEY `fk_progetto_3_idx` (`anno_accademico`),
  CONSTRAINT `fk_progetto_1` FOREIGN KEY (`codice_corso`) REFERENCES `corso` (`idcorso`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_progetto_2` FOREIGN KEY (`email_docente`) REFERENCES `professore` (`email_professore`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_progetto_3` FOREIGN KEY (`anno_accademico`) REFERENCES `corso` (`idcorso`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `studente`
--

DROP TABLE IF EXISTS `studente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `studente` (
  `email_studente` varchar(45) NOT NULL,
  `data_nascita` date NOT NULL,
  `luogo_nascita` varchar(45) NOT NULL,
  `anno_immatricolazione` int(10) NOT NULL,
  `informatica` tinyint(1) NOT NULL,
  `informatica_management` tinyint(1) NOT NULL,
  PRIMARY KEY (`email_studente`),
  CONSTRAINT `fk_studente_1` FOREIGN KEY (`email_studente`) REFERENCES `utente` (`email`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `utente`
--

DROP TABLE IF EXISTS `utente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `utente` (
  `email` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `nome` varchar(45) NOT NULL,
  `cognome` varchar(45) NOT NULL,
  `foto` mediumblob,
  PRIMARY KEY (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-04-27 19:18:22
