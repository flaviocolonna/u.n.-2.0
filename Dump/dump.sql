CREATE DATABASE  IF NOT EXISTS `unibo_project` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `unibo_project`;
-- MySQL dump 10.13  Distrib 5.6.19, for linux-glibc2.5 (x86_64)
--
-- Host: 127.0.0.1    Database: unibo_project
-- ------------------------------------------------------
-- Server version	5.6.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `allegato`
--

DROP TABLE IF EXISTS `allegato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `allegato` (
  `idallegato` int(11) NOT NULL,
  `idprogetto` int(11) DEFAULT NULL,
  `tipo` varchar(45) DEFAULT NULL,
  `file` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idallegato`),
  KEY `fk_allegato_1_idx` (`idprogetto`),
  CONSTRAINT `fk_allegato_1` FOREIGN KEY (`idprogetto`) REFERENCES `progetto` (`idprogetto`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `allegato`
--

LOCK TABLES `allegato` WRITE;
/*!40000 ALTER TABLE `allegato` DISABLE KEYS */;
/*!40000 ALTER TABLE `allegato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `appunti`
--

DROP TABLE IF EXISTS `appunti`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `appunti` (
  `idappunti` int(11) NOT NULL,
  `idcreatore` int(11) NOT NULL,
  `descrizione` varchar(45) NOT NULL,
  `data` varchar(45) NOT NULL,
  `allegato` varchar(45) NOT NULL,
  `tipo_allegato` varchar(45) NOT NULL,
  PRIMARY KEY (`idappunti`),
  KEY `fk_appunti_1_idx` (`idcreatore`),
  CONSTRAINT `fk_appunti_1` FOREIGN KEY (`idcreatore`) REFERENCES `studente` (`matricola`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `appunti`
--

LOCK TABLES `appunti` WRITE;
/*!40000 ALTER TABLE `appunti` DISABLE KEYS */;
/*!40000 ALTER TABLE `appunti` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `commenti`
--

DROP TABLE IF EXISTS `commenti`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `commenti` (
  `idcommento` int(11) NOT NULL,
  `idappunto` int(11) NOT NULL,
  `idcreatore` int(11) NOT NULL,
  `testo` varchar(45) NOT NULL,
  `data` varchar(45) NOT NULL,
  `valutazione` varchar(45) NOT NULL,
  PRIMARY KEY (`idcommento`),
  KEY `fk_commenti_1_idx` (`idcreatore`),
  KEY `fk_commenti_2_idx` (`idappunto`),
  CONSTRAINT `fk_commenti_1` FOREIGN KEY (`idcreatore`) REFERENCES `studente` (`matricola`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_commenti_2` FOREIGN KEY (`idappunto`) REFERENCES `appunti` (`idappunti`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `commenti`
--

LOCK TABLES `commenti` WRITE;
/*!40000 ALTER TABLE `commenti` DISABLE KEYS */;
/*!40000 ALTER TABLE `commenti` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `corr_progetto`
--

DROP TABLE IF EXISTS `corr_progetto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `corr_progetto` (
  `idstudente` int(11) NOT NULL,
  `idprogetto` int(11) DEFAULT NULL,
  PRIMARY KEY (`idstudente`),
  KEY `fk_corr_progetto_1_idx` (`idprogetto`),
  CONSTRAINT `fk_corr_progetto_1` FOREIGN KEY (`idprogetto`) REFERENCES `progetto` (`idprogetto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_corr_progetto_2` FOREIGN KEY (`idstudente`) REFERENCES `studente` (`matricola`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `corr_progetto`
--

LOCK TABLES `corr_progetto` WRITE;
/*!40000 ALTER TABLE `corr_progetto` DISABLE KEYS */;
/*!40000 ALTER TABLE `corr_progetto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `corso`
--

DROP TABLE IF EXISTS `corso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `corso` (
  `idcorso` int(11) NOT NULL,
  `idprofessore` int(11) DEFAULT NULL,
  `codice_corso` varchar(45) DEFAULT NULL,
  `anno_accademico` varchar(45) DEFAULT NULL,
  `nome_corso` varchar(45) DEFAULT NULL,
  `numero_crediti` varchar(45) DEFAULT NULL,
  `corso_laurea` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idcorso`),
  KEY `fk_corso_1_idx` (`idprofessore`),
  CONSTRAINT `fk_corso_1` FOREIGN KEY (`idprofessore`) REFERENCES `professore` (`idprofessore`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `corso`
--

LOCK TABLES `corso` WRITE;
/*!40000 ALTER TABLE `corso` DISABLE KEYS */;
/*!40000 ALTER TABLE `corso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elaborato`
--

DROP TABLE IF EXISTS `elaborato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elaborato` (
  `idelaborato` int(11) NOT NULL,
  `idesercitazione` int(11) NOT NULL,
  `idstudente` int(11) NOT NULL,
  `commento` varchar(45) NOT NULL,
  `allegato` varchar(45) NOT NULL,
  `data_upload` varchar(45) NOT NULL,
  PRIMARY KEY (`idelaborato`),
  KEY `fk_elaborato_1_idx` (`idesercitazione`),
  KEY `fk_elaborato_2_idx` (`idstudente`),
  CONSTRAINT `fk_elaborato_1` FOREIGN KEY (`idesercitazione`) REFERENCES `esercitazione` (`idesercitazione`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_elaborato_2` FOREIGN KEY (`idstudente`) REFERENCES `studente` (`matricola`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elaborato`
--

LOCK TABLES `elaborato` WRITE;
/*!40000 ALTER TABLE `elaborato` DISABLE KEYS */;
/*!40000 ALTER TABLE `elaborato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `esercitazione`
--

DROP TABLE IF EXISTS `esercitazione`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `esercitazione` (
  `idesercitazione` int(11) NOT NULL,
  `idprofessore` int(11) NOT NULL,
  `idcorso` int(11) NOT NULL,
  `descrizione` varchar(45) NOT NULL,
  `data_inizio` date NOT NULL,
  `data_fine` date NOT NULL,
  `voto` varchar(45) NOT NULL,
  PRIMARY KEY (`idesercitazione`),
  KEY `fk_esercitazioni_1_idx` (`idprofessore`),
  KEY `fk_esercitazioni_2_idx` (`idcorso`),
  CONSTRAINT `fk_esercitazioni_1` FOREIGN KEY (`idprofessore`) REFERENCES `professore` (`idprofessore`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_esercitazioni_2` FOREIGN KEY (`idcorso`) REFERENCES `corso` (`idcorso`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `esercitazione`
--

LOCK TABLES `esercitazione` WRITE;
/*!40000 ALTER TABLE `esercitazione` DISABLE KEYS */;
/*!40000 ALTER TABLE `esercitazione` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `follower`
--

DROP TABLE IF EXISTS `follower`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `follower` (
  `idfollowed` int(11) NOT NULL,
  `idfollower` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `follower`
--

LOCK TABLES `follower` WRITE;
/*!40000 ALTER TABLE `follower` DISABLE KEYS */;
/*!40000 ALTER TABLE `follower` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inbox`
--

DROP TABLE IF EXISTS `inbox`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inbox` (
  `idmessaggio` int(11) NOT NULL,
  `idmittente` int(11) NOT NULL,
  `subject` varchar(45) NOT NULL,
  `contenuto` varchar(45) NOT NULL,
  `data_invio` date NOT NULL,
  PRIMARY KEY (`idmessaggio`),
  KEY `fk_inbox_1_idx` (`idmittente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inbox`
--

LOCK TABLES `inbox` WRITE;
/*!40000 ALTER TABLE `inbox` DISABLE KEYS */;
/*!40000 ALTER TABLE `inbox` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lista_aziende`
--

DROP TABLE IF EXISTS `lista_aziende`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lista_aziende` (
  `idazienda` int(11) NOT NULL AUTO_INCREMENT,
  `idprogetto` int(11) NOT NULL,
  `nome_azienda` varchar(45) NOT NULL,
  `sito` varchar(45) NOT NULL,
  `indirizzo` varchar(45) NOT NULL,
  PRIMARY KEY (`idazienda`),
  KEY `fk_lista_aziende_1_idx` (`idprogetto`),
  CONSTRAINT `fk_lista_aziende_1` FOREIGN KEY (`idprogetto`) REFERENCES `progetto` (`idprogetto`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lista_aziende`
--

LOCK TABLES `lista_aziende` WRITE;
/*!40000 ALTER TABLE `lista_aziende` DISABLE KEYS */;
/*!40000 ALTER TABLE `lista_aziende` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news` (
  `idnews` int(11) NOT NULL,
  `idprofessore` int(11) DEFAULT NULL,
  `subject` varchar(45) DEFAULT NULL,
  `stringa_testo` varchar(45) DEFAULT NULL,
  `data_inserimento` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idnews`),
  KEY `fk_news_1_idx` (`idprofessore`),
  CONSTRAINT `fk_news_1` FOREIGN KEY (`idprofessore`) REFERENCES `professore` (`idprofessore`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news`
--

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `professore`
--

DROP TABLE IF EXISTS `professore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `professore` (
  `idprofessore` int(11) NOT NULL,
  `nome` varchar(45) NOT NULL,
  `cognome` varchar(45) NOT NULL,
  `foto` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `ruolo` varchar(45) NOT NULL,
  PRIMARY KEY (`idprofessore`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `professore`
--

LOCK TABLES `professore` WRITE;
/*!40000 ALTER TABLE `professore` DISABLE KEYS */;
/*!40000 ALTER TABLE `professore` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `progetto`
--

DROP TABLE IF EXISTS `progetto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `progetto` (
  `idprogetto` int(11) NOT NULL,
  `nome` varchar(45) NOT NULL,
  `descrizione` varchar(45) NOT NULL,
  `data_inizio` varchar(45) NOT NULL,
  `num_partecipanti` varchar(45) NOT NULL,
  `tipo` varchar(45) NOT NULL,
  `idcorso` int(11) DEFAULT NULL,
  `voto` varchar(45) DEFAULT NULL COMMENT 'Questa tabella è inerente ai progetti. Ogni progetto ha dei dettagli comuni ma poi possono esserci due tipi di progetti: universitari ed extra.',
  `categoria` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idprogetto`),
  KEY `fk_progetto_1_idx` (`idcorso`),
  CONSTRAINT `fk_progetto_1` FOREIGN KEY (`idcorso`) REFERENCES `corso` (`idcorso`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `progetto`
--

LOCK TABLES `progetto` WRITE;
/*!40000 ALTER TABLE `progetto` DISABLE KEYS */;
/*!40000 ALTER TABLE `progetto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `studente`
--

DROP TABLE IF EXISTS `studente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `studente` (
  `matricola` int(11) NOT NULL,
  `email` varchar(45) NOT NULL,
  `nome` varchar(45) NOT NULL,
  `cognome` varchar(45) NOT NULL,
  `foto` varchar(45) NOT NULL,
  PRIMARY KEY (`matricola`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `studente`
--

LOCK TABLES `studente` WRITE;
/*!40000 ALTER TABLE `studente` DISABLE KEYS */;
/*!40000 ALTER TABLE `studente` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-02-24 20:41:07
