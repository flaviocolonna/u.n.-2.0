var main = function(){
	$('select').material_select();
	$('.datepicker').pickadate({
    	selectMonths: true,
    	selectYears: 50,
    	format: 'yyyy-mm-dd',
    	closeOnSelect: true,
    	min: new Date(1951,0,0),
  		max: new Date(1996,11,31)
  	});
  	$('#role').on('change',function(){
  		var valore = $('#role').val();
  		if(valore==='1'){ //professore
			$('#ruolo').show();
			$('#corso_laurea').hide();
			$('.student').hide();
  		}else if (valore==='2'){ //studente
			$('#ruolo').hide();
			$('#corso_laurea').show();
			$('.student').show();
  		}
  	});
	//funzione per evitare che l'immagine di sfondo diventi draggable
	$('img').on('dragstart', function(event) { event.preventDefault(); });
	//funzione che gestisce il submit del form
	$('#registration-form').submit(function(event){
		var valoreOp = $('#role').val();
		//controllo quale utente intende registrarsi
		if(valoreOp != null && valoreOp==='2'){
			var immatricolazione = $('#immatricolazione').val();
			var luogo_nascita = $('#placebirth').val();
			var data_nascita=$('#birthday').val();
			var corso_laurea = $('#corso_laurea select').val();
			if(immatricolazione==null || luogo_nascita==null || data_nascita==null || corso_laurea==null){
				$('#modal1').openModal();
				$('#modal-title').html('Errore');
				$('#modal1 p').html('Alcuni campi presentano valori nulli!');
				event.preventDefault();
			}else{
				$('#op').val('0');
			}
		}else if(valoreOp!=null && valoreOp==='1'){
			var ruolo = $('#ruolo select').val();
			if(ruolo==null){
				$('#modal1').openModal();
				$('#modal-title').html('Errore');
				$('#modal1 p').html('Inserire il proprio ruolo da professore!');
				event.preventDefault();
			}else{
				$('#op').val('1');
			}
		}else if(valoreOp==null || valoreOp==='0'){
			$('#modal1').openModal();
			$('#modal1 p').html('Selezionare il tipo di utente che intende registrarsi!');
			$('#modal-title').html('Errore');
			event.preventDefault();
		}
	});
};
$(document).ready(main);