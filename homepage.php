<?php
session_start();
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<link rel="shortcut icon" href="/images/favicon.ico" type="image/x-icon">
		<title>University Network</title>
		<link rel="stylesheet" href="/css/materialize.min.css" />
		<link rel="stylesheet" href="/css/style.css" />
		<script src="/lib/jquery-1.10.2.min.js"></script>
	</head>
	<body>
		<div class="container">
			<nav>
    			<div class="nav-wrapper">
    				<ul id="nav-mobile" class="left">
    						<li>
    						Ciao, <?php
			        echo $_SESSION['nome'].' '.$_SESSION['cognome']; ?>
    						</li>
    					</ul>
			      <ul id="nav-mobile" class="right hide-on-med-and-down">
			      	<li><a href="homepage.php">Home</a></li>
			        <li><a class="clickable">Messaggi</a></li>
			        <li><a class="clickable">Profilo</a></li>
			        <li><a class="clickable">Impostazioni</a></li>
			      </ul>
		    	</div>
		 	</nav>
		 	<?php
		 	if($_SESSION['welcome']):
		 		$_SESSION['welcome']=false;
		 	?>
		 		<div class="modal" id="modal1">
				<div class="modal-content">
					<h4 id="modal-title">Benvenuto!</h4>
					<p>Questa è la prima volta che accedi in questo portale.</p>
					<p>Da qui potrai gestire i tuoi impegni universitari, che tu sia studente o professore.</p>
					<p>Potrai inviare messaggi, rimanere aggiornate sulle notizie dei docenti e partecipare ai progetti.</p>
				</div>
				<div class="modal-footer">
					<a class="modal-action modal-close waves-effect waves-green btn-flat">Chiudi</a>
				</div>
			</div>
			<script type="text/javascript">
				$(document).ready(function(){
					$('#modal1').openModal({
						dismissible:true
					});
				});
			</script>
		 	<?php
		 	endif;
		 	?>
		</div>
		
		<script type="text/javascript" src="/lib/materialize.min.js"></script>
		<script type="text/javascript" src="/scripts/app.js"></script>
	</body>
</html>